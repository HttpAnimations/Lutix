# Lutix/stable
A GUI tool for XMR.

## Installing
```bash
echo "No install command right now."
```

## Reason?
This was made beacuse I did not like the [Gupax](https://gupax.io/) UI that's it :3.

## Credits
1) [QT](https://doc.qt.io/)
2) [p2pool](https://p2pool.io/)
3) [XMRIG](https://xmrig.com/)
3) [Gupax](https://gupax.io/)